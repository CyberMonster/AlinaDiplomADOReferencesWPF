﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ADODiplomWPF.Views
{
    public partial class ErrorHandler : Window
    {
        public ErrorHandler(string CancelButton, string OkButton, string ErrorText)
        {
            InitializeComponent();
            this.OkButton.Focus();
            this.CancelButton.Content = CancelButton;
            this.OkButton.Content = OkButton;
            this.ErrorText.AppendText(ErrorText);
        }
        /// <summary>
        /// Обработчик нажатия. Продолжить работу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            if (DialogResult != null)
            {
                this.DialogResult = true;
            }
            this.Close();
        }
        /// <summary>
        /// Обработчик нажатия. Завершить работу
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (DialogResult != null)
            {
                this.DialogResult = false;
            }
            this.Close();
        }
    }
}
