﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using ADOMainApp.MySqlFunctions;
using MahApps.Metro.Controls;

namespace ADODiplomWPF.Views
{
    /// <summary>
    /// Логика взаимодействия для MainWorkSpace.xaml
    /// </summary>
    public partial class MainWorkSpace : MetroWindow
    {
        /// <summary>
        /// Конструктор поиска по отделениям
        /// </summary>
        /// <param name="Otdelenie"></param>
        /// <param name="IdOtdelenie"></param>
        public MainWorkSpace(string Otdelenie, string IdOtdelenie)
        {
            InitializeComponent();
            this.IdOtdelenie = IdOtdelenie;
            this.Title = Otdelenie;
            MainInit(true);
        }
        /// <summary>
        /// Конструктор общего поиска
        /// </summary>
        public MainWorkSpace()
        {
            InitializeComponent();
            this.Title = @"Общий поиск";
            MainInit(false);
        }
        /// <summary>
        /// Главный инициализатор
        /// </summary>
        /// <param name="IsNeedOtdel"></param>
        private void MainInit(bool IsNeedOtdel)
        {
            this.CommonQuery = @"SELECT ID_student as 'Идентификатор студента', Fam AS 'Фамилия', Ima AS 'Имя',Otch AS 'Отчество', Sokr_nazv AS 'Пол', Data_rogd AS 'Дата рождения', Lich_delo AS '№ личного дела',
                            Kod_gruppa AS 'Идентификатор группы', Student.Telephone AS '№ телефона',
                            CONCAT(Sostoyaniye.Nazvanie, ' ', Sostoyaniye.Dopolnitelno) AS 'Состояние обучения', ID_kurs AS 'Курс обучения'
                            FROM `Student` JOIN `Pol` ON Pol.ID_pol = Student.ID_pol JOIN `Gruppa` ON Gruppa.ID_gruppa = Student.ID_gruppa JOIN `Sostoyaniye` ON Sostoyaniye.ID_sost = Student.ID_sost
                            JOIN `Forma_obuch` ON Forma_obuch.ID_forma = Student.ID_forma JOIN `Obrazovanie` ON Obrazovanie.ID_obraz = Student.ID_obraz
                            JOIN `Otdelenie` ON Otdelenie.ID_otdel = Student.ID_otdel JOIN `Roditely` ON Roditely.ID_roditel = Student.ID_roditel
                            JOIN `Dokumenty` ON Dokumenty.ID_dok = Student.ID_dok" + (IsNeedOtdel ? @" WHERE Student.ID_otdel = " + this.IdOtdelenie : @"");
            this.CommonQueryEndler = @" ORDER BY ID_student;";

            ListsInitialization();

            var Data = DBConnector.GetDate(this.CommonQuery + this.CommonQueryEndler).DefaultView;
            this.MainDataPresenter.DataContext = Data;
            this.LoadedElementsCount.Text = Data.Count.ToString();
        }
        /// <summary>
        /// Инициализация второй части ListView
        /// </summary>
        private void ListsInitialization()
        {
            (this.Filters.Items[0] as CheckBox).IsChecked = true;

            ListViewBufferedData = new List<CheckBoxContextAndIsChecked>();

            ListViewBufferedData.Add(new CheckBoxContextAndIsChecked() { Content = (this.Filters.Items[0] as CheckBox).Content.ToString(), IsChecked = (this.Filters.Items[0] as CheckBox).IsChecked });
            ListViewBufferedData.Add(new CheckBoxContextAndIsChecked() { Content = (this.Filters.Items[8] as CheckBox).Content.ToString(), IsChecked = (this.Filters.Items[8] as CheckBox).IsChecked });
        }
        /// <summary>
        /// Обработчик нажатия клавиш в строке поиска.
        /// Функция формирования и выполнения нового
        /// запроса, согласно выбранным фильтрам.
        /// </summary>
        /// <param name="sender">TextBox</param>
        /// <param name="e">Параметры</param>
        private void MakeSearch(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string Query = "";
                var SearchItems = this.SearchQuery.Text.Replace("/;", "{/_}").Split(';').Select(x => x.Replace("{/_}", ";")).ToArray();
                int SearchItemId = 0;
                if (this.QueryModificator.IsChecked == true)
                {
                    foreach (var CheckedItem in this.Filters.Items)
                    {
                        if ((CheckedItem as CheckBox).IsChecked == true)
                        {
                            switch (this.Filters.Items.IndexOf(CheckedItem))
                            {
                                case 0:
                                    var FIO = SearchItems[SearchItemId].Replace("/ ", "{/_}").Split(' ').Select(x => x.Replace("{/_}", " ")).ToArray();
                                    switch (FIO.Length)
                                    {
                                        case 1:
                                            Query += @" AND Roditely.FIO like '%" + FIO[0] + "%'";
                                            break;
                                        case 2:
                                            Query += @" AND Roditely.FIO like '%" + FIO[0] + @"%' AND Roditely.FIO like '%" + FIO[1] + "%'";
                                            break;
                                        case 3:
                                            Query += @" AND Roditely.FIO like '%" + FIO[0] + @"%' Roditely.FIO like '%" + FIO[1] + @"%' AND Roditely.FIO like '%" + FIO[2] + "%'";
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                                case 1:
                                    Query += @" AND Roditely.Telephone like '%" + SearchItems[SearchItemId] + "%'";
                                    break;
                                default:
                                    break;
                            }
                        }
                        ++SearchItemId;
                        if (SearchItemId == SearchItems.Length)
                        {
                            --SearchItemId;
                        }
                    }
                }
                else
                {
                    foreach (var CheckedItem in this.Filters.Items)
                    {
                        if ((CheckedItem as CheckBox).IsChecked == true)
                        {
                            switch (this.Filters.Items.IndexOf(CheckedItem))
                            {
                                case 0:
                                    var FIO = SearchItems[SearchItemId].Replace("/ ", "_").Split(' ').Select(x => x.Replace("_", " ")).ToArray();
                                    switch (FIO.Length)
                                    {
                                        case 1:
                                            Query += @" AND `Fam` like '%" + FIO[0] + "%'";
                                            break;
                                        case 2:
                                            Query += @" AND `Fam` like '%" + FIO[0] + @"%' AND Student.Ima like '%" + FIO[1] + "%'";
                                            break;
                                        case 3:
                                            Query += @" AND `Fam` like '%" + FIO[0] + @"%' AND Student.Ima like '%" + FIO[1] + @"%' AND Student.Otch like '%" + FIO[2] + "%'";
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                                case 1:
                                    Query += @" AND Student.Ima like '%" + SearchItems[SearchItemId] + "%'";
                                    break;
                                case 2:
                                    Query += @" AND Student.Otch like '%" + SearchItems[SearchItemId] + "%'";
                                    break;
                                case 3:
                                    Query += @" AND gruppa.Kod_gruppa like '%" + SearchItems[SearchItemId] + "%'";
                                    break;
                                case 4:
                                    Query += @" AND Student.ID_kurs like '%" + SearchItems[SearchItemId] + "%'";
                                    break;
                                case 5:
                                    Query += @" AND ID_godpost like '%" + SearchItems[SearchItemId] + "%'";
                                    break;
                                case 6:
                                    var PassData = SearchItems[SearchItemId].Split(' ');
                                    switch (PassData.Length)
                                    {
                                        case 1:
                                            Query += @" AND Dokumenty.Seriya_pasp like '%" + PassData[0] + "%'";
                                            break;
                                        case 2:
                                            Query += @" AND Dokumenty.Seriya_pasp like '%" + PassData[0] + @"%' AND Dokumenty.Number_pasp like '%" + PassData[1] + "%'";
                                            break;
                                        default:
                                            break;
                                    }
                                    break;
                                case 7:
                                    Query += @" AND Dokumenty.Number_pasp like '%" + SearchItems[SearchItemId] + "%'";
                                    break;
                                case 8:
                                    Query += @" AND Student.Telephone like '%" + SearchItems[SearchItemId] + "%'";
                                    break;
                                default:
                                    break;
                            }
                        }
                        ++SearchItemId;
                        if (SearchItemId == SearchItems.Length)
                        {
                            --SearchItemId;
                        }
                    }
                }
                if (Query != "")
                {
                    this.MainDataPresenter.DataContext = DBConnector.GetDate(this.CommonQuery + Query + this.CommonQueryEndler).DefaultView;
                    this.LoadedElementsCount.Text = this.MainDataPresenter.Items.Count.ToString();
                }
            }
        }
        /// <summary>
        /// Обработчик события смены состояния в CheckBox.
        /// Функция подменяет содержимое в ListView,
        /// а следовательно и фильтры.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QueryModificator_Checked(object sender, RoutedEventArgs e)
        {
            if (IsCheckedBeforeEvent != (sender as CheckBox).IsChecked)
            {
                this.CheckBoxBufferMainData = new List<CheckBoxContextAndIsChecked>();
                foreach (object Item in this.Filters.Items)
                {
                    CheckBoxBufferMainData.Add(new CheckBoxContextAndIsChecked() { Content = (Item as CheckBox).Content.ToString(), IsChecked = (Item as CheckBox).IsChecked });
                }
                this.Filters.Items.Clear();
                foreach (CheckBoxContextAndIsChecked Item in ListViewBufferedData)
                {
                    this.Filters.Items.Add(new CheckBox() { Content = Item.Content, IsChecked = Item.IsChecked });
                }
                ListViewBufferedData.Clear();
                ListViewBufferedData.AddRange(CheckBoxBufferMainData);
                CheckBoxBufferMainData.Clear();

                IsCheckedBeforeEvent = !IsCheckedBeforeEvent;
            }
        }
        /// <summary>
        /// Номер отделения
        /// </summary>
        private string IdOtdelenie;
        /// <summary>
        /// Общая часть запроса
        /// </summary>
        public string CommonQuery;
        /// <summary>
        /// Сортировка списка
        /// </summary>
        public string CommonQueryEndler;
        /// <summary>
        /// Буффер хранения параметров поиска
        /// </summary>
        private List<CheckBoxContextAndIsChecked> CheckBoxBufferMainData;
        /// <summary>
        /// Буффер хранения параметров поиска
        /// </summary>
        private List<CheckBoxContextAndIsChecked> ListViewBufferedData;
        /// <summary>
        /// Параметр смены фильтров
        /// </summary>
        private bool IsCheckedBeforeEvent = false;
        /// <summary>
        /// Обработчик события редактирования эллемента
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecuteDetailView(object sender, MouseButtonEventArgs e)
        {
            if ((sender as DataGrid).CurrentItem is System.Data.DataRowView)
            {
                new DetailInfo(((sender as DataGrid).CurrentItem as System.Data.DataRowView), this).Show();
            }
        }
        /// <summary>
        /// Обработчик события двойного клика в строке.
        /// Открывает окно редактирования.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddNewRow(object sender, RoutedEventArgs e)
        {
            new DetailInfo().Show();
        }
        /// <summary>
        /// метод обновления таблицы
        /// </summary>
        /// <param name="EditebleRow"></param>
        public void RefreshRow(System.Data.DataRowView EditebleRow)
        {
            this.MainDataPresenter.DataContext = DBConnector.GetDate(this.CommonQuery + this.CommonQueryEndler).DefaultView;
        }
        /// <summary>
        /// Обработчик события. Выдает на печать содержимое видимой части таблицы данных
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Print_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog printDlg = new PrintDialog();
            if ((bool)printDlg.ShowDialog().GetValueOrDefault())
            {
                Size pagesize = new Size(printDlg.PrintableAreaWidth, printDlg.PrintableAreaHeight);
                this.MainDataPresenter.Measure(pagesize);
                this.MainDataPresenter.Arrange(new Rect(5, 5, pagesize.Width, pagesize.Height));
                printDlg.PrintVisual(this.MainDataPresenter, "Personal Information");
            }
        }
    }

    class CheckBoxContextAndIsChecked
    {
        /// <summary>
        /// Активен ли checkbox
        /// </summary>
        private bool? IsChecked_ = false;
        public bool? IsChecked
        {
            get
            {
                return this.IsChecked_;
            }
            set
            {
                this.IsChecked_ = value;
            }
        }
        /// <summary>
        /// Содержимое checkbox'а
        /// </summary>
        private string Content_ = @"";
        public string Content
        {
            get
            {
                return this.Content_;
            }
            set
            {
                this.Content_ = value;
            }
        }
    }
}