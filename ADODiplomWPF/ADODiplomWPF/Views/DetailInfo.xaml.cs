﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

using ADOMainApp.MySqlFunctions;

using ADOMainApp.DataTypes;

namespace ADODiplomWPF.Views
{
    /// <summary>
    /// Логика взаимодействия для DetailInfo.xaml
    /// </summary>
    public partial class DetailInfo : Window
    {
        /// <summary>
        /// Конструктор при редактировании
        /// </summary>
        /// <param name="SelectedRow">Идентификатор студента</param>
        public DetailInfo(System.Data.DataRowView SelectedRow, MainWorkSpace Sender)
        {
            InitializeComponent();

            this.EditebleRow = SelectedRow;
            this.ParentWindow = Sender;

            this.MainContainer.DataContext = this;
            this.StudentInfo = new StudentInformation(SelectedRow.Row.ItemArray[0].ToString());
            SetValueToComboboxControls();
        }
        /// <summary>
        /// Конструктор при добавлении
        /// </summary>
        public DetailInfo()
        {
            InitializeComponent();

            this.MainContainer.DataContext = this;
            this.StudentInfo = new StudentInformation();
        }
        /// <summary>
        /// Задание значений для Combobox'ов
        /// </summary>
        private void SetValueToComboboxControls()
        {
            this.Pol.SelectedIndex = this.StudentInfo.Pol.Select(x => x.ID_pol).ToList().IndexOf(StudentInfo.PolSelected.ID_pol);
            this.Gruppa.SelectedIndex = this.StudentInfo.Gruppa.Select(x => x.ID_gruppa).ToList().IndexOf(StudentInfo.GruppaSelected.ID_gruppa);
            this.Sost.SelectedIndex = this.StudentInfo.Sost.Select(x => x.ID_sost).ToList().IndexOf(StudentInfo.SostSelected.ID_sost);
            this.Kurs.SelectedIndex = this.StudentInfo.Kurs.Select(x => x.ID_kurs).ToList().IndexOf(StudentInfo.KyrsSelected.ID_kurs);
            this.Godpost.SelectedIndex = this.StudentInfo.Godpost.Select(x => x.ID_godpost).ToList().IndexOf(StudentInfo.GodpostSelected.ID_godpost);
            this.Forma.SelectedIndex = this.StudentInfo.Forma.Select(x => x.ID_forma).ToList().IndexOf(StudentInfo.FormaSelected.ID_forma);
            this.Otdelenie.SelectedIndex = this.StudentInfo.Otdelenie.Select(x => x.ID_Otdel).ToList().IndexOf(StudentInfo.OtdelenieSelected.ID_Otdel);
            this.Education.SelectedIndex = this.StudentInfo.Education.Select(x => x.ID_obraz).ToList().IndexOf(StudentInfo.EducationSelected.ID_obraz);
        }
        /// <summary>
        /// Информация о студенте
        /// </summary>
        public StudentInformation StudentInfo { get; set; }
        /// <summary>
        /// Редактируемая запись
        /// </summary>
        private System.Data.DataRowView EditebleRow {get;set;}
        /// <summary>
        /// Родительское окно
        /// </summary>
        private MainWorkSpace ParentWindow { get; set; }
        /// <summary>
        /// Обработчик события нажатия кнопки сохранения
        /// </summary>
        /// <param name="sender">Кнопка</param>
        /// <param name="e">Параметры</param>
        private void SaveButtonClick(object sender, RoutedEventArgs e)
        {
            if (StudentInfo.GodpostSelected == null)
            {
                StudentInfo.GodpostSelected = new GodPost() { ID_godpost = this.Godpost.Text };
                DBConnector.UpdateDate(@"INSERT INTO `God_post`(`ID_godpost`) VALUES ('" + this.StudentInfo.GodpostSelected.ID_godpost + @"');");
            }
            if (StudentInfo.StudentID == null)
            {
                StudentInfo.Add();
            }
            else
            {
                StudentInfo.Update();
                this.ParentWindow.RefreshRow(this.EditebleRow);
            }
            this.Close();
        }
        /// <summary>
        /// процедура удаления записи
        /// </summary>
        /// <param name="sender">Row</param>
        /// <param name="e">EventArgs</param>
        private void DeleteRow(object sender, RoutedEventArgs e)
        {
            this.StudentInfo.DeleteRow();
            this.ParentWindow.RefreshRow(this.EditebleRow);
            this.Close();
        }
        /// <summary>
        /// Валидатор целочисленных полей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnlyNumbersForTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = Regex.IsMatch(e.Text, @"[^0-9]+");
        }
        /// <summary>
        /// Валидатор полей, предназначенных для ввода телефонных номеров
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PhoneValidator(object sender, TextCompositionEventArgs e)
        {
            if ((sender as TextBox).Text.Length == 0)
            {
                e.Handled = !Regex.IsMatch(e.Text, @"\+");
                if (e.Handled)
                {
                    e.Handled = Regex.IsMatch(e.Text, @"[^0-9]");
                }
            }
            else
            {
                e.Handled = Regex.IsMatch(e.Text, @"[^0-9]+");
            }
        }
    }
}
