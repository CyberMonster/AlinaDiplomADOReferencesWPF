﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using ADOMainApp;
using ADOMainApp.MySqlFunctions;
using ADOMainApp.AppData;

using MahApps.Metro.Controls;

namespace ADODiplomWPF.Views
{
    /// <summary>
    /// Логика взаимодействия для Login.xaml
    /// </summary>
    public partial class Login : MetroWindow
    {
        public Login()
        {
            InitializeComponent();
            this.LoginControl.Focus();
        }
        /// <summary>
        /// Метод проверки введенной пары ключ/значение
        /// </summary>
        private void TestKeyValueAndOpenOtherWindows()
        {
            ConnectionInfo.MakeConnect(this.LoginControl.Text, this.Password.Password);
            /// <summary>
            /// Проверка правельности введеной пары Логин/Пароль
            /// </summary>
            if (DBConnector.GetDate(@"SELECT * FROM `Otdelenie`").Rows.Count > 0)
            {
                new MainWindow().Show();
                this.Close();
            }
            else
            {
                var ErrorDialog = new ErrorHandler(@"Отмена", @"Ок", @"Неверное имя пользователя или пароль!");
                ErrorDialog.ShowDialog();
            }
        }
        /// <summary>
        /// Обработчик событий нажатия клавиши
        /// </summary>
        /// <param name="sender">TextBox</param>
        /// <param name="e">Параметры</param>
        private void FormInputKeyDownEventHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TestKeyValueAndOpenOtherWindows();
            }
        }
        /// <summary>
        /// Обработчик события нажатия кнопки подключения
        /// </summary>
        /// <param name="sender">Кнопка</param>
        /// <param name="e">Параметры</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            TestKeyValueAndOpenOtherWindows();
        }
    }
}
