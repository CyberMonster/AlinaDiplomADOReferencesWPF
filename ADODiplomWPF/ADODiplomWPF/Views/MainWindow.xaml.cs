﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using ADOMainApp.MySqlFunctions;
using ADOMainApp.DataTypes;
using System.Diagnostics;

namespace ADODiplomWPF.Views
{
    /// <summary>
    /// Логика взаимодействия для MainPresentation.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            ListViewInitialize();
        }
        /// <summary>
        /// Функция инициализации ListView со списком отделений
        /// </summary>
        private void ListViewInitialize()
        {
            this.OtdelenieList = new List<OtdelenieDataClass>();

            var RowsResult = DBConnector.GetDate(@"SELECT * FROM `Otdelenie`").Rows;
            for (var i = 0; i < RowsResult.Count; ++i)
            {
                this.OtdelenieList.Add(new OtdelenieDataClass(RowsResult[i].ItemArray[0].ToString(), RowsResult[i].ItemArray[1].ToString(), RowsResult[i].ItemArray[2].ToString(), RowsResult[i].ItemArray[3].ToString()));
            }

            this.ListView1.ItemsSource = this.OtdelenieList;
        }
        /// <summary>
        /// Список отделений, отображаемых на форме
        /// </summary>
        private List<OtdelenieDataClass> OtdelenieList;
        /// <summary>
        /// Обработчик нажатия кнапки в ListView
        /// </summary>
        /// <param name="sender">ListView</param>
        /// <param name="e">Параметры</param>
        private void OtdelSelected(object sender, RoutedEventArgs e)
        {
            new MainWorkSpace((((sender as Button).Parent as Grid).Children[0] as Button).Content.ToString(), (((sender as Button).Parent as Grid).Children[1] as Label).Content.ToString()).Show();
        }
        private void AddNewRow(object sender, RoutedEventArgs e)
        {
            new DetailInfo().Show();
        }
        /// <summary>
        /// Обработчик перехода на главный сайт
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainSite(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo(new Uri(@"http://mtkp.ru/").AbsoluteUri));
        }
        /// <summary>
        /// Обработчик перехода на вкладку "Администрация"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Administration(object sender, RoutedEventArgs e)
        {
            Process.Start(new ProcessStartInfo(new Uri(@"http://mtkp.ru/about/administration/").AbsoluteUri));
        }
        /// <summary>
        /// Обработчик перехода на форму "О программе"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void About(object sender, RoutedEventArgs e)
        {
            new AboutPage().Show();
        }
        /// <summary>
        /// Событие перехода на форму
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FullSearch(object sender, RoutedEventArgs e)
        {
            new MainWorkSpace().Show();
        }
    }
}
