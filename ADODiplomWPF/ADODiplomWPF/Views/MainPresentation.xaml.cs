﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using ADOMainApp.MySqlFunctions;
using ADOMainApp.DataTypes;

namespace ADODiplomWPF.Views
{
    /// <summary>
    /// Логика взаимодействия для MainPresentation.xaml
    /// </summary>
    public partial class MainPresentation : Window
    {
        public MainPresentation()
        {
            InitializeComponent();
            ListViewInitialize();
        }
        /// <summary>
        /// Функция инициализации ListView со списком отделений
        /// </summary>
        private void ListViewInitialize()
        {
            this.OtdelenieList = new List<OtdelenieDataClass>();

            var RowsResult = DBConnector.GetDate(@"SELECT * FROM `Otdelenie`").Rows;
            for (var i = 0; i < RowsResult.Count; ++i)
            {
                this.OtdelenieList.Add(new OtdelenieDataClass(RowsResult[i].ItemArray[0].ToString(), RowsResult[i].ItemArray[1].ToString(), RowsResult[i].ItemArray[2].ToString(), RowsResult[i].ItemArray[3].ToString()));
            }

            this.ListView1.ItemsSource = this.OtdelenieList;
        }

        private List<OtdelenieDataClass> OtdelenieList;
        /// <summary>
        /// Обработчик нажатия кнапки в ListView
        /// </summary>
        /// <param name="sender">ListView</param>
        /// <param name="e">Параметры</param>
        private void OtdelSelected(object sender, RoutedEventArgs e)
        {
            new MainWorkSpace((((sender as Button).Parent as Grid).Children[0] as Button).Content.ToString(), (((sender as Button).Parent as Grid).Children[1] as Label).Content.ToString()).Show();
        }
    }
}
