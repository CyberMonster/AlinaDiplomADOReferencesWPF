﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data;
using MySql.Data.MySqlClient;
using ADODiplomWPF.Views;

using ADOMainApp.AppData;

namespace ADOMainApp.MySqlFunctions
{
    /// <summary>
    /// Класс для работы с базой данных. Содержит
    /// методы для выполнения запросов.
    /// </summary>
    static class DBConnector
    {
        /// <summary>
        /// Выполняет соединение с базой данных
        /// </summary>
        /// <param name="ServerIP"> IP of remote server </param>
        /// <param name="DataBaseName"> Name of needed database </param>
        /// <param name="UserName"> User name </param>
        /// <param name="UserPassword"> User password </param>
        public static MySqlConnectionStringBuilder Connect(string ServerIP, string DataBaseName, string UserName, string UserPassword)
        {
            return new MySqlConnectionStringBuilder() { Server = ServerIP, Database = DataBaseName, UserID = UserName, Password = UserPassword };
        }
        /// <summary>
        /// Переменная таблицы данных
        /// </summary>
        private static System.Data.DataTable ResultTable;
        /// <summary>
        /// Функция получения строк из базы данных
        /// </summary>
        /// <param name="Query">Запрос к базе данных</param>
        /// <returns></returns>
        public static System.Data.DataTable GetDate(string Query)
        {
            ConnectionInfo.MakeConnect();
            ResultTable = new System.Data.DataTable();
            using (MySqlConnection Connect = new MySqlConnection())
            {
                Connect.ConnectionString = ConnectionInfo.DBConnection.ConnectionString;
                MySqlCommand Command = new MySqlCommand(Query, Connect);
                try
                {
                    Connect.Open();
                    using (MySqlDataReader DataReader = Command.ExecuteReader())
                    {
                        if (DataReader.HasRows)
                        {
                            ResultTable.Load(DataReader);
                        }
                    }
                }
                catch (Exception ex)
                {
                    new ErrorHandler(@"Отмена", @"Ок", @"Error in query: " + Query + @"       Error Message: " + ex.Message + @"       Stack: " + ex.StackTrace).Show();
                }
                Connect.Close();
            }
            return ResultTable;
        }
        /// <summary>
        /// Функция выполнения запроса, без получения строк
        /// </summary>
        /// <param name="Query">Запрос к базе данных</param>
        public static void UpdateDate(string Query)
        {
            ConnectionInfo.MakeConnect();
            using (MySqlConnection Connect = new MySqlConnection())
            {
                Connect.ConnectionString = ConnectionInfo.DBConnection.ConnectionString;
                MySqlCommand Command = new MySqlCommand(Query, Connect);
                try
                {
                    Command.Connection.Open();
                    Command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    new ErrorHandler(@"Отмена", @"Ок", @"Error in query: " + Query + @"       Error Message: " + ex.Message + @"       Stack: " + ex.StackTrace).Show();
                }
                Connect.Close();
            }
        }
    }
}