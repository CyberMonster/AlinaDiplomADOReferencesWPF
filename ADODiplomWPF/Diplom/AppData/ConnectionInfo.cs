﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data;
using MySql.Data.MySqlClient;

using ADOMainApp.MySqlFunctions;

using ADODiplomWPF;
using ADODiplomWPF.Resources;

namespace ADOMainApp.AppData
{
    class ConnectionInfo
    {
        /// <summary>
        /// IPv4 сервера бд
        /// </summary>
        public static string ServerIP = ServerInfo.DBUrl;
        /// <summary>
        /// Имя базы данных
        /// </summary>
        public static string DataBaseName = ServerInfo.DBName;
        /// <summary>
        /// Имя пользователя
        /// </summary>
        public static string UserName;
        /// <summary>
        /// Пароль пользователя
        /// </summary>
        public static string UserPassword;
        /// <summary>
        /// Строка подключения к БД
        /// </summary>
        public static MySqlConnectionStringBuilder DBConnection;
        /// <summary>
        /// Функция подключения к БД
        /// </summary>
        public static void MakeConnect()
        {
            DBConnection = DBConnector.Connect(
                ServerIP, DataBaseName,
                UserName, UserPassword);
            DBConnection.Add(@"Convert Zero Datetime", true);
        }
        /// <summary>
        /// Функция подключения к БД
        /// </summary>
        /// <param name="UserName_">Имя пользователя</param>
        /// <param name="UserPassword_">Пароль пользователя</param>
        /// <returns>Строка подключения к БД</returns>
        public static MySqlConnectionStringBuilder MakeConnect(string UserName_, string UserPassword_)
        {
            UserName = UserName_;
            UserPassword = UserPassword_;
            MakeConnect();
            return DBConnection;
        }
        /// <summary>
        /// Функция получения IPv4 адреса через DNS по URL
        /// </summary>
        /// <param name="URL"></param>
        public static void SetServerIPViaURL(string URL)
        {
            ServerIP = System.Net.Dns.GetHostAddresses(URL)[0].MapToIPv4().Address.ToString();
        }
    }
}