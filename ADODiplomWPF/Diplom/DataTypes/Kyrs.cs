﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADOMainApp.DataTypes
{
    public class Kyrs
    {
        /// <summary>
        /// Получения списка курсов
        /// </summary>
        public static string Query = @"SELECT* FROM `Kurs`";
        /// <summary>
        /// Получение курса по идентификатору
        /// </summary>
        public static string GetCurrentQuery = @"SELECT* FROM `Kurs` WHERE `ID_kurs` = '";
        public Kyrs(object[] Items)
        {
            this.ID_kurs = Items[0].ToString();
        }
        public Kyrs()
        {
            ;
        }
        /// <summary>
        /// Идентификатор курса
        /// </summary>
        public string ID_kurs { get; set; }
    }
}
