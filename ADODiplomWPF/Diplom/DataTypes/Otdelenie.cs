﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADOMainApp.DataTypes
{
    public class Otdelenie
    {
        /// <summary>
        /// Получение списка отделений
        /// </summary>
        public static string Query = @"SELECT * FROM `Otdelenie`";
        /// <summary>
        /// Получение отделения по идентификатору
        /// </summary>
        public static string GetCurrentQuery = @"SELECT * FROM `Otdelenie` WHERE `ID_Otdel` = '";
        public Otdelenie(object[] Items)
        {
            this.ID_Otdel = Items[0].ToString();
            this.Nazvanie = Items[1].ToString();
            this.FIO_zav = Items[2].ToString();
            this.Telephone = Items[3].ToString();
        }
        public Otdelenie()
        {
            ;
        }
        /// <summary>
        /// Идентификатор отделения
        /// </summary>
        public string ID_Otdel { get; set; }
        /// <summary>
        /// Название отделения
        /// </summary>
        public string Nazvanie { get; set; }
        /// <summary>
        /// ФИО заведующей
        /// </summary>
        public string FIO_zav { get; set; }
        /// <summary>
        /// Номер телефона
        /// </summary>
        public string Telephone { get; set; }
    }
}
