﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADOMainApp.DataTypes
{
    public class Gruppa
    {
        /// <summary>
        /// Получение списка групп
        /// </summary>
        public static string Query = @"SELECT * FROM `Gruppa`";
        /// <summary>
        /// Получение данных о группе
        /// </summary>
        public static string GetCurrentQuery = @"SELECT * FROM `Gruppa` WHERE `ID_gruppa` = '";
        public Gruppa(object[] Items)
        {
            this.ID_gruppa = Items[0].ToString();
            this.Kod_gruppa = Items[1].ToString();
            this.ID_spec = Items[2].ToString();
            this.ID_klruk = Items[3].ToString();
        }
        public Gruppa()
        {
            ;
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public string ID_gruppa { get; set; }
        /// <summary>
        /// Код группы
        /// </summary>
        public string Kod_gruppa { get; set; }
        /// <summary>
        /// Идентификатор специальности
        /// </summary>
        public string ID_spec { get; set; }
        /// <summary>
        /// Идентификатор классного руководителя
        /// </summary>
        public string ID_klruk { get; set; }
    }
}
