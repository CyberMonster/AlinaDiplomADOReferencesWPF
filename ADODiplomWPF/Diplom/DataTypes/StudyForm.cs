﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADOMainApp.DataTypes
{
    public class StudyForm
    {
        /// <summary>
        /// Получение списка Форм обучения
        /// </summary>
        public static string Query = @"SELECT * FROM `Forma_obuch`";
        /// <summary>
        /// Получение формы обучения по идентификатору
        /// </summary>
        public static string GetCurrentQuery = @"SELECT * FROM `Forma_obuch` WHERE `ID_forma` = '";
        public StudyForm(object[] Items)
        {
            this.ID_forma = Items[0].ToString();
            this.Nazvanie = Items[1].ToString();
        }
        public StudyForm()
        {
            ;
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public string ID_forma { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        public string Nazvanie { get; set; }
    }
}
