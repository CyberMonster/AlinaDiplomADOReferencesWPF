﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOMainApp.DataTypes
{
    public class PolDataClass
    {
        public PolDataClass(int MaleUID, string Male)
        {
            this.MaleUID = MaleUID;
            this.Male = Male;
        }

        public int MaleUID { get; set; }
        public string Male { get; set; }
    }
}