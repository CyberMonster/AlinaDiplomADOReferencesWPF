﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADOMainApp.DataTypes
{
    public class Document
    {
        /// <summary>
        /// Запрос на получение данных
        /// </summary>
        public static string Query = @"SELECT * FROM `Dokumenty` WHERE ID_dok = '";
        public Document(object[] Items)
        {
            this.ID_dok = Items[0].ToString();
            this.Number_pasp = Items[1].ToString();
            this.Seriya_pasp = Items[2].ToString();
            this.Gragdanstvo = Items[3].ToString();
            this.UMS = Items[4].ToString() == @"+" ? true : false;
            this.Vydach_pasp = Items[5].ToString();
            this.Kod_podrazdel = Items[6].ToString();
        }
        public Document()
        {
            ;
        }
        /// <summary>
        /// Идентификатор документов
        /// </summary>
        public string ID_dok { get; set; }
        /// <summary>
        /// Номер паспорта
        /// </summary>
        public string Number_pasp { get; set; }
        /// <summary>
        /// Серия паспорта
        /// </summary>
        public string Seriya_pasp { get; set; }
        /// <summary>
        /// Гражданство
        /// </summary>
        public string Gragdanstvo { get; set; }
        /// <summary>
        /// УМС
        /// </summary>
        public bool UMS { get; set; }
        /// <summary>
        /// Кем выдан паспорт
        /// </summary>
        public string Vydach_pasp { get; set; }
        /// <summary>
        /// Код подразделения выдачи
        /// </summary>
        public string Kod_podrazdel { get; set; }
    }
}
