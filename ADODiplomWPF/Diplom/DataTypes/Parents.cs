﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADOMainApp.DataTypes
{
    public class Parents
    {
        /// <summary>
        /// Получение данных о родителях
        /// </summary>
        public static string Query = @"SELECT * FROM `Roditely` WHERE `ID_roditel` = '";
        public Parents(object[] Items)
        {
            this.ID_roditel = Items[0].ToString();
            this.FIO = Items[1].ToString();
            this.Telephone = Items[2].ToString();
        }
        public Parents()
        {
            ;
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public string ID_roditel { get; set; }
        /// <summary>
        /// ФИО
        /// </summary>
        public string FIO { get; set; }
        /// <summary>
        /// Номер телефона
        /// </summary>
        public string Telephone { get; set; }
    }
}
