﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADOMainApp.DataTypes
{
    public class Adress
    {
        /// <summary>
        /// Запрос на получение адреса
        /// </summary>
        public static string Query = @"SELECT * FROM ";
        public Adress(object[] Items)
        {
            this.IDAdress = Items[0].ToString();
            this.Indeks = Items[1].ToString();
            this.Gorod = Items[2].ToString();
            this.Ulica = Items[3].ToString();
            this.Dom = Items[4].ToString();
            this.Kvartira = Items[5].ToString();
        }
        public Adress()
        {
            ;
        }
        /// <summary>
        /// Идентификатор адреса
        /// </summary>
        public string IDAdress { get; set; }
        /// <summary>
        /// Почтовый индекс
        /// </summary>
        public string Indeks { get; set; }
        /// <summary>
        /// Город
        /// </summary>
        public string Gorod { get; set; }
        /// <summary>
        /// Улица
        /// </summary>
        public string Ulica { get; set; }
        /// <summary>
        /// Дом
        /// </summary>
        public string Dom { get; set; }
        /// <summary>
        /// Квартира
        /// </summary>
        public string Kvartira { get; set; }
    }
}
