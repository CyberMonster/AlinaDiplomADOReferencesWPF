﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

using ADOMainApp.MySqlFunctions;
using ADOMainApp.DataTypes;
using ADODiplomWPF.Views;

namespace ADOMainApp.DataTypes
{
    /// <summary>
    /// Информация о студенте
    /// </summary>
    public class StudentInformation
    {
        /// <summary>
        /// Конструктор, вызываемый при добавлении нового стулента
        /// </summary>
        public StudentInformation()
        {
            LoadLists();
            InitFields();
        }
        /// <summary>
        /// Конструктор при редактировании
        /// </summary>
        /// <param name="StudentID"></param>
        public StudentInformation(string StudentID)
        {
            this.StudentID = StudentID;
            this.Query = @"SELECT * FROM `Student` WHERE Student.ID_student = " + this.StudentID;
            SetDataFromDB();
        }
        /// <summary>
        /// Функция, заполняющая поля данными
        /// </summary>
        private void SetDataFromDB()
        {
            System.Data.DataTable QueryResult = DBConnector.GetDate(this.Query);
            if (QueryResult.Rows.Count > 0)
            {
                var Items = QueryResult.Rows[0].ItemArray;
                LoadLists();
                this.StudentID = Items[0].ToString();
                this.Fam = Items[1].ToString();
                this.Ima = Items[2].ToString();
                this.Otch = Items[3].ToString();
                this.PolSelected = new Gender(DBConnector.GetDate(Gender.GetCurrentQuery + Items[4].ToString() + @"'").Rows[0].ItemArray);
                this.Data_rogd = Items[5].ToString();
                this.Lich_delo = Items[6].ToString();
                this.GruppaSelected = new Gruppa(DBConnector.GetDate(DataTypes.Gruppa.GetCurrentQuery + Items[7].ToString() + @"'").Rows[0].ItemArray);
                this.Telephone = Items[8].ToString();
                this.SostSelected = new StateOfStuding(DBConnector.GetDate(StateOfStuding.GetCurrentQuery + Items[9].ToString() + @"'").Rows[0].ItemArray);
                this.KyrsSelected = new Kyrs(DBConnector.GetDate(Kyrs.GetCurrentQuery + Items[10].ToString() + @"'").Rows[0].ItemArray);
                this.GodpostSelected = new GodPost(DBConnector.GetDate(GodPost.GetCurrentQuery + Items[11].ToString() + @"'").Rows[0].ItemArray);
                this.Document = new Document(DBConnector.GetDate(Document.Query + Items[12].ToString() + @"'").Rows[0].ItemArray);
                this.adresreg = new Adress(DBConnector.GetDate(Adress.Query + @"`Adres_reg` WHERE `ID_adresreg` = '" + Items[13].ToString() + @"'").Rows[0].ItemArray);
                this.adresprog = new Adress(DBConnector.GetDate(Adress.Query + @"`Adres_prog` WHERE `ID_adresprog` = '" + Items[14].ToString() + @"'").Rows[0].ItemArray);
                this.FormaSelected = new StudyForm(DBConnector.GetDate(StudyForm.GetCurrentQuery + Items[15].ToString() + @"'").Rows[0].ItemArray);
                this.Roditeli = new Parents(DBConnector.GetDate(Parents.Query + Items[16].ToString() + @"'").Rows[0].ItemArray);
                this.OtdelenieSelected = new Otdelenie(DBConnector.GetDate(DataTypes.Otdelenie.GetCurrentQuery + Items[17].ToString() + @"'").Rows[0].ItemArray);
                this.EducationSelected = new Education(DBConnector.GetDate(DataTypes.Education.GetCurrentQuery + Items[18].ToString() + @"'").Rows[0].ItemArray);
            }
        }
        /// <summary>
        /// Инициализатор полей
        /// </summary>
        private void InitFields()
        {
            this.PolSelected = new Gender();
            this.GruppaSelected = new Gruppa();
            this.SostSelected = new StateOfStuding();
            this.KyrsSelected = new Kyrs();
            this.GodpostSelected = new GodPost();
            this.Document = new Document();
            this.adresreg = new Adress();
            this.adresprog = new Adress();
            this.FormaSelected = new StudyForm();
            this.Roditeli = new Parents();
            this.OtdelenieSelected = new Otdelenie();
            this.EducationSelected = new Education();
        }
        /// <summary>
        /// Функция формирования, и отправки запроса на обновление данных
        /// </summary>
        public void Update()
        {
            try
            {
                var Buffer = this.Data_rogd.Split(' ')[0].Split('.');
                this.UpdateQuery = @"UPDATE `Student` SET `ID_student`='" + this.StudentID + @"',
                                    `Fam`='" + this.Fam + @"',`Ima`='" + this.Ima + @"',
                                    `Otch`='" + this.Otch + @"',`ID_pol`='" + this.PolSelected.ID_pol + @"',
                                    `Data_rogd`='" + Buffer[2] + '.' + Buffer[1] + '.' + Buffer[0] + @"',
                                    `Lich_delo`='" + this.Lich_delo + @"',`ID_gruppa`='" + this.GruppaSelected.ID_gruppa + @"',
                                    `Telephone`='" + this.Telephone + @"',`ID_sost`='" + this.SostSelected.ID_sost + @"',
                                    `ID_kurs`='" + this.KyrsSelected.ID_kurs + @"',`ID_godpost`='" + this.GodpostSelected.ID_godpost + @"',
                                    `ID_dok`='" + this.Document.ID_dok + @"',`ID_adresreg`='" + this.adresreg.IDAdress + @"',
                                    `ID_adresprog`='" + this.adresprog.IDAdress + @"',`ID_forma`='" + this.FormaSelected.ID_forma + @"',
                                    `ID_roditel`='" + this.Roditeli.ID_roditel + @"',`ID_otdel`='" + this.OtdelenieSelected.ID_Otdel + @"',
                                    `ID_obraz`='" + this.EducationSelected.ID_obraz + @"' WHERE ID_student = '" + this.StudentID + @"';
                                    UPDATE `Dokumenty` SET `ID_dok`='" + this.Document.ID_dok + @"',`Number_pasp`='" + this.Document.Number_pasp + @"',
                                    `Seriya_pasp`='" + this.Document.Seriya_pasp + @"',`Gragdanstvo`='" + this.Document.Gragdanstvo + @"',
                                    `UMS`='" + (this.Document.UMS ? @"+" : @"-") + @"',`Vydach_pasp`='" + this.Document.Vydach_pasp + @"',
                                    `Kod_podrazdel`='" + this.Document.Kod_podrazdel + @"' WHERE `ID_dok` = '" + this.Document.ID_dok + @"';
                                    UPDATE `Adres_reg` SET `ID_adresreg`='" + this.adresreg.IDAdress + @"',`Indeks`='" + this.adresreg.Indeks + @"',
                                    `Gorod`='" + this.adresreg.Gorod + @"',`Ulica`='" + this.adresreg.Ulica + @"',`Dom`='" + this.adresreg.Dom + @"',
                                    `Kvartira`='" + this.adresreg.Kvartira + @"' WHERE `ID_adresreg` = '" + this.adresreg.IDAdress + @"';
                                    UPDATE `Adres_prog` SET `ID_adresprog`='" + this.adresprog.IDAdress + @"',`Indeks`='" + this.adresprog.IDAdress + @"',
                                    `Gorod`='" + this.adresprog.Gorod + @"',`Ulica`='" + this.adresprog.Ulica + @"',`Dom`='" + this.adresprog.Dom + @"',
                                    `Kvartira`='" + this.adresprog.Kvartira + @"' WHERE `ID_adresprog` = '" + this.adresprog.IDAdress + @"';
                                    UPDATE `Roditely` SET `ID_roditel`='" + this.Roditeli.ID_roditel + @"',`FIO`='" + this.Roditeli.FIO + @"',
                                    `Telephone`='" + this.Roditeli.Telephone + @"' WHERE `ID_roditel` = '" + this.Roditeli.ID_roditel + @"';";
            }
            catch (Exception ex)
            {
                new ErrorHandler(@"Отмена", @"Ок", ex.Message).ShowDialog();
            }
            DBConnector.UpdateDate(this.UpdateQuery);
        }
        /// <summary>
        /// Функция формирования, и отправки запроса на добавление данных
        /// </summary>
        public void Add()
        {
            var Buffer = this.Data_rogd.Split(' ')[0].Split('.');
            this.Document.ID_dok = DBConnector.GetDate(@"SELECT MAX(`ID_dok`) + 1 FROM `Dokumenty`").Rows[0].ItemArray[0].ToString();
            this.adresreg.IDAdress = DBConnector.GetDate(@"SELECT MAX(`ID_adresreg`) + 1 FROM `Adres_reg`").Rows[0].ItemArray[0].ToString();
            this.adresprog.IDAdress = DBConnector.GetDate(@"SELECT MAX(`ID_adresprog`) + 1 FROM `Adres_prog`").Rows[0].ItemArray[0].ToString();
            this.Roditeli.ID_roditel = DBConnector.GetDate(@"SELECT MAX(`ID_roditel`) + 1 FROM `Roditely`").Rows[0].ItemArray[0].ToString();
            this.StudentID = DBConnector.GetDate(@"SELECT MAX(`ID_student`) + 1 FROM `Student`").Rows[0].ItemArray[0].ToString();
            this.UpdateQuery = @"INSERT INTO `Dokumenty`(`ID_dok`, `Number_pasp`, `Seriya_pasp`,
                                `Gragdanstvo`, `UMS`, `Vydach_pasp`, `Kod_podrazdel`) VALUES (
                                '" + this.Document.ID_dok + @"','" + this.Document.Number_pasp + @"',
                                '" + this.Document.Seriya_pasp + @"','" + this.Document.Gragdanstvo + @"',
                                '" + (this.Document.UMS ? @"+" : @"-") + @"','" + this.Document.Vydach_pasp + @"',
                                '" + this.Document.Kod_podrazdel + @"');
                                INSERT INTO `Adres_reg`(`ID_adresreg`, `Indeks`, `Gorod`, `Ulica`,
                                `Dom`, `Kvartira`) VALUES ('" + this.adresreg.IDAdress + @"',
                                '" + this.adresreg.Indeks + @"','" + this.adresreg.Gorod + @"',
                                '" + this.adresreg.Ulica + @"','" + this.adresreg.Dom + @"',
                                '" + this.adresreg.Kvartira + @"');
                                INSERT INTO `Adres_prog`(`ID_adresprog`, `Indeks`, `Gorod`, `Ulica`,
                                `Dom`, `Kvartira`) VALUES ('" + this.adresprog.IDAdress + @"',
                                '" + this.adresprog.Indeks + @"','" + this.adresprog.Gorod + @"',
                                '" + this.adresprog.Ulica + @"','" + this.adresprog.Dom + @"',
                                '" + this.adresprog.Kvartira + @"');
                                INSERT INTO `Roditely`(`ID_roditel`, `FIO`, `Telephone`) VALUES (
                                '" + this.Roditeli.ID_roditel + @"','" + this.Roditeli.FIO + @"',
                                '" + this.Roditeli.Telephone + @"');
                                INSERT INTO `Student`(`ID_student`, `Fam`, `Ima`, `Otch`,
                                `ID_pol`, `Data_rogd`, `Lich_delo`, `ID_gruppa`, `Telephone`,
                                `ID_sost`, `ID_kurs`, `ID_godpost`, `ID_dok`, `ID_adresreg`,
                                `ID_adresprog`, `ID_forma`, `ID_roditel`, `ID_otdel`,
                                `ID_obraz`) VALUES ('" + this.StudentID + @"',
                                '" + this.Fam + @"', '" + this.Ima + @"', '" + this.Otch + @"',
                                '" + this.PolSelected.ID_pol + @"', '" + Buffer[2] + '.' + Buffer[1] + '.' + Buffer[0] + @"',
                                '" + this.Lich_delo + @"', '" + this.GruppaSelected.ID_gruppa + @"',
                                '" + this.Telephone + @"','" + this.SostSelected.ID_sost + @"',
                                '" + this.KyrsSelected.ID_kurs + @"', '" + this.GodpostSelected.ID_godpost + @"',
                                '" + this.Document.ID_dok + @"', '" + this.adresreg.IDAdress + @"',
                                '" + this.adresprog.IDAdress + @"', '" + this.FormaSelected.ID_forma + @"',
                                '" + this.Roditeli.ID_roditel + @"', '" + this.OtdelenieSelected.ID_Otdel + @"',
                                '" + this.EducationSelected.ID_obraz + @"')";
            DBConnector.UpdateDate(this.UpdateQuery);
        }
        /// <summary>
        /// Удаление строки
        /// </summary>
        public void DeleteRow()
        {
            try
            {
                DBConnector.UpdateDate(@"DELETE FROM `Dokumenty` WHERE `ID_dok` = '" + this.Document.ID_dok + @"'");
                DBConnector.UpdateDate(@"DELETE FROM `Adres_reg` WHERE `ID_adresreg` = '" + this.adresreg.IDAdress + @"'");
                DBConnector.UpdateDate(@"DELETE FROM `Adres_prog` WHERE `ID_adresprog` = '" + this.adresprog.IDAdress + @"'");
                DBConnector.UpdateDate(@"DELETE FROM `Roditely` WHERE `ID_roditel` = '" + this.Roditeli.ID_roditel + @"'");
                DBConnector.UpdateDate(@"DELETE FROM `Student` WHERE `ID_student` = '" + this.StudentID + @"'");
            }
            catch (Exception ex)
            {
                new ErrorHandler(@"Отмена", @"Ок", ex.Message).ShowDialog();
            }
        }
        /// <summary>
        /// Функция загрузки данных в выпадающие списки
        /// </summary>
        private void LoadLists()
        {
            this.Pol = new List<Gender>();
            QueryResultBuffer = DBConnector.GetDate(Gender.Query);
            for (var i = 0; i < QueryResultBuffer.Rows.Count; ++i)
            {
                this.Pol.Add(new Gender(QueryResultBuffer.Rows[i].ItemArray));
            }

            this.Gruppa = new List<Gruppa>();
            QueryResultBuffer = DBConnector.GetDate(DataTypes.Gruppa.Query);
            for (var i = 0; i < QueryResultBuffer.Rows.Count; ++i)
            {
                this.Gruppa.Add(new Gruppa(QueryResultBuffer.Rows[i].ItemArray));
            }

            this.Sost = new List<StateOfStuding>();
            QueryResultBuffer = DBConnector.GetDate(StateOfStuding.Query);
            for (var i = 0; i < QueryResultBuffer.Rows.Count; ++i)
            {
                this.Sost.Add(new StateOfStuding(QueryResultBuffer.Rows[i].ItemArray));
            }

            this.Kurs = new List<Kyrs>();
            QueryResultBuffer = DBConnector.GetDate(Kyrs.Query);
            for (var i = 0; i < QueryResultBuffer.Rows.Count; ++i)
            {
                this.Kurs.Add(new Kyrs(QueryResultBuffer.Rows[i].ItemArray));
            }

            this.Godpost = new List<GodPost>();
            QueryResultBuffer = DBConnector.GetDate(GodPost.Query);
            for (var i = 0; i < QueryResultBuffer.Rows.Count; ++i)
            {
                this.Godpost.Add(new GodPost(QueryResultBuffer.Rows[i].ItemArray));
            }

            this.Forma = new List<StudyForm>();
            QueryResultBuffer = DBConnector.GetDate(StudyForm.Query);
            for (var i = 0; i < QueryResultBuffer.Rows.Count; ++i)
            {
                this.Forma.Add(new StudyForm(QueryResultBuffer.Rows[i].ItemArray));
            }

            this.Otdelenie = new List<Otdelenie>();
            QueryResultBuffer = DBConnector.GetDate(DataTypes.Otdelenie.Query);
            for (var i = 0; i < QueryResultBuffer.Rows.Count; ++i)
            {
                this.Otdelenie.Add(new Otdelenie(QueryResultBuffer.Rows[i].ItemArray));
            }

            this.Education = new List<Education>();
            QueryResultBuffer = DBConnector.GetDate(DataTypes.Education.Query);
            for (var i = 0; i < QueryResultBuffer.Rows.Count; ++i)
            {
                this.Education.Add(new Education(QueryResultBuffer.Rows[i].ItemArray));
            }
        }
        /// <summary>
        /// Переменная запроса
        /// </summary>
        private string Query;
        private string SelectedData_rogd
        {
            get;
            set;
        }
        /// <summary>
        /// Вспомогательная переменная запроса
        /// </summary>
        private string UpdateQuery;
        /// <summary>
        /// Переменная результата запроса
        /// </summary>
        private System.Data.DataTable QueryResultBuffer;
        /// <summary>
        /// выбранный пол
        /// </summary>
        public Gender PolSelected { get; set; }
        /// <summary>
        /// Выбранная группа
        /// </summary>
        public Gruppa GruppaSelected { get; set; }
        /// <summary>
        /// Выбранное состояние обучения
        /// </summary>
        public StateOfStuding SostSelected { get; set; }
        /// <summary>
        /// Выбранный курс
        /// </summary>
        public Kyrs KyrsSelected { get; set; }
        /// <summary>
        /// Выбранный год поступления
        /// </summary>
        public GodPost GodpostSelected { get; set; }
        /// <summary>
        /// Выбранная форма обучения
        /// </summary>
        public StudyForm FormaSelected { get; set; }
        /// <summary>
        /// Выбранное отделение
        /// </summary>
        public Otdelenie OtdelenieSelected { get; set; }
        /// <summary>
        /// Выбранный уровень образования
        /// </summary>
        public Education EducationSelected { get; set; }
        /// <summary>
        /// Идентификатор студента
        /// </summary>
        public string StudentID { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Fam { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string Ima { get; set; }
        /// <summary>
        /// Отчество
        /// </summary>
        public string Otch { get; set; }
        /// <summary>
        /// Список полов
        /// </summary>
        public List<Gender> Pol { get; set; }
        /// <summary>
        /// Дата рождения
        /// </summary>
        public string Data_rogd { get; set; }
        /// <summary>
        /// Номер личного дела
        /// </summary>
        public string Lich_delo { get; set; }
        /// <summary>
        /// Список групп
        /// </summary>
        public List<Gruppa> Gruppa { get; set; }
        /// <summary>
        /// Номер телефона
        /// </summary>
        public string Telephone { get; set; }
        /// <summary>
        /// Список состояний
        /// </summary>
        public List<StateOfStuding> Sost { get; set; }
        /// <summary>
        /// Список курсов
        /// </summary>
        public List<Kyrs> Kurs { get; set; }
        /// <summary>
        /// Список лет поступления
        /// </summary>
        public List<GodPost> Godpost { get; set; }
        /// <summary>
        /// Документы
        /// </summary>
        public Document Document { get; set; }
        /// <summary>
        /// Адрес регистрации
        /// </summary>
        public Adress adresreg { get; set; }
        /// <summary>
        /// Адрес проживания
        /// </summary>
        public Adress adresprog { get; set; }
        /// <summary>
        /// Список форм обучения
        /// </summary>
        public List <StudyForm> Forma { get; set; }
        /// <summary>
        /// Данные о родителях
        /// </summary>
        public Parents Roditeli { get; set; }
        /// <summary>
        /// Список отделений
        /// </summary>
        public List<Otdelenie> Otdelenie { get; set; }
        /// <summary>
        /// Список уровней образования
        /// </summary>
        public List<Education> Education { get; set; }
    }
}