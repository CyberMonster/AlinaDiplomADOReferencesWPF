﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADOMainApp.DataTypes
{
    class OtdelenieDataClass
    {
        public OtdelenieDataClass(string UIDOtdel, string Title, string FIOResponseblePerson, string PhoneNumber)
        {
            this.UIDOtdel = UIDOtdel;
            this.Nazvanie = Title;
            this.FIO_zav = FIOResponseblePerson;
            this.Telephone = PhoneNumber;
        }

        public string UIDOtdel { get; set; }
        public string Nazvanie { get; set; }
        public string FIO_zav { get; set; }
        public string Telephone { get; set; }
    }
}