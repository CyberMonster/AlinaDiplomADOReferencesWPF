﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADOMainApp.DataTypes
{
    public class Education
    {
        /// <summary>
        /// Запрос на получение списка
        /// </summary>
        public static string Query = @"SELECT * FROM `Obrazovanie`";
        /// <summary>
        /// Запрос на получение записи
        /// </summary>
        public static string GetCurrentQuery = @"SELECT * FROM `Obrazovanie` WHERE `ID_obraz` = '";
        public Education(object[] Items)
        {
            this.ID_obraz = Items[0].ToString();
            this.Nazvanie = Items[1].ToString();
        }
        public Education()
        {
            ;
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public string ID_obraz { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        public string Nazvanie { get; set; }
    }
}
