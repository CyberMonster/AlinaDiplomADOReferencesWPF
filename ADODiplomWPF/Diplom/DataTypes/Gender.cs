﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADOMainApp.DataTypes
{
    public class Gender
    {
        /// <summary>
        /// Получение списка полов
        /// </summary>
        public static string Query = @"SELECT * FROM `Pol`";
        /// <summary>
        /// Получение записи
        /// </summary>
        public static string GetCurrentQuery = @"SELECT * FROM `Pol` WHERE `ID_pol` = '";
        public Gender(object[] Items)
        {
            this.ID_pol = Items[0].ToString();
            this.Nazvanie = Items[1].ToString();
            this.Sokr_nazv = Items[2].ToString();
        }
        public Gender()
        {
            ;
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public string ID_pol { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        public string Nazvanie { get; set; }
        /// <summary>
        /// Сокращенное название
        /// </summary>
        public string Sokr_nazv { get; set; }
    }
}
