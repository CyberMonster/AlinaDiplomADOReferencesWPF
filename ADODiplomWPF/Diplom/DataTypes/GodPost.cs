﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADOMainApp.DataTypes
{
    public class GodPost
    {
        /// <summary>
        /// Получение списка лет поступлений
        /// </summary>
        public static string Query = @"SELECT * FROM `God_post`";
        /// <summary>
        /// Получение записи
        /// </summary>
        public static string GetCurrentQuery = @"SELECT * FROM `God_post` WHERE `ID_godpost` = '";
        public GodPost(object[] Items)
        {
            ID_godpost = Items[0].ToString();
        }
        public GodPost()
        {
            ;
        }
        /// <summary>
        /// Идентификатор года поступления
        /// </summary>
        public string ID_godpost { get; set; }
    }
}
