﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADOMainApp.DataTypes
{
    public class StateOfStuding
    {
        /// <summary>
        /// Получение списка состояний
        /// </summary>
        public static string Query = @"SELECT * FROM `Sostoyaniye`";
        /// <summary>
        /// Получение состояния
        /// </summary>
        public static string GetCurrentQuery = @"SELECT * FROM `Sostoyaniye` WHERE `ID_sost` = '";

        public StateOfStuding(object[] Items)
        {
            this.ID_sost = Items[0].ToString();
            this.Nazvanie = Items[1].ToString();
            this.Dopolnitelno = Items[2].ToString();
            this.ItemForDisplay = Nazvanie;
            if (this.Dopolnitelno != null && this.Dopolnitelno != @"")
            {
                this.ItemForDisplay += @" (" + this.Dopolnitelno + @")";
            }
        }
        public StateOfStuding()
        {
            ;
        }
        /// <summary>
        /// Идентификатор
        /// </summary>
        public string ID_sost { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        public string Nazvanie { get; set; }
        /// <summary>
        /// Дополнительное название
        /// </summary>
        public string Dopolnitelno { get; set; }
        /// <summary>
        /// Отображаемый эллемент
        /// </summary>
        public string ItemForDisplay { get; set; }
    }
}
